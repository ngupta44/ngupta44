package com.csc.sics.mtk.etl.util;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.csc.sics.mtk.etl.io.AbstractLookupTable;
import com.csc.sics.mtk.etl.io.OdbcRecord;

public class CommonUtils {

	public static final String sectionName = "Main Section";
	public static final String y = "Y";
	public static final String n = "N";
	public static String getString(final OdbcRecord odbcRecord, final String fieldName, final String defaultValue) {
		return isNullOrBlank(odbcRecord, fieldName) ? defaultValue.trim() : odbcRecord.getString(fieldName).trim();
	}

	public static String getString(final OdbcRecord odbcRecord, final String fieldName) {
		return isNull(odbcRecord, fieldName) ? "" : odbcRecord.getString(fieldName).trim();
	}

	public static boolean isNullOrBlank(final OdbcRecord odbcRecord, final String fieldName) {
		return odbcRecord.getField(fieldName) == null || odbcRecord.getField(fieldName).equals("");
	}

	public static boolean isNull(final OdbcRecord odbcRecord, final String fieldName) {
		return odbcRecord.getField(fieldName) == null;
	}

	public static Calendar addNineteenHundredToYearOfDate(final Timestamp date) {
		Calendar cal = Calendar.getInstance();
		if (date == null) {
			cal = null;
		} else {
			cal.setTimeInMillis(date.getTime());
			cal.add(Calendar.YEAR, 1900);
		}
		return cal;
	}

	public static boolean isDateInBetween(final Date date, final Date startDate, final Date endDate) {
		return date.compareTo(startDate) > 0 && date.compareTo(endDate) < 0 ? true : false;
	}

	public static Calendar calendar(final Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	public static Date getIpEndDate(final OdbcRecord odbcRecord, final OdbcRecord nextRecord) {
		Calendar calendar = Calendar.getInstance();
		if (nextRecord != null) {
			calendar.setTime(nextRecord.getTimestamp("CVF_DATE"));
			calendar.add(Calendar.DATE, -1);

		} else {
			calendar.setTime(odbcRecord.getTimestamp("CVF_DATE"));
			calendar.add(Calendar.YEAR, 1);
			calendar.add(Calendar.DATE, -1);
		}
		return calendar.getTime();
	}

	public static List<OdbcRecord> sortList(final List<OdbcRecord> list, final String fieldName) {

		Collections.sort(list, new Comparator<OdbcRecord>() {

			@Override
			public int compare(final OdbcRecord o1, final OdbcRecord o2) {
				if (Integer.parseInt(o1.getField(fieldName).toString()) == Integer.parseInt(o2.getField(fieldName).toString())) {
					return 0;
				} else if (Integer.parseInt(o1.getField(fieldName).toString()) < Integer.parseInt(o2.getField(fieldName).toString())) {
					return -1;
				} else {
					return 1;
				}
			}
		});
		return list;
	}

	public static void sortListByDate(final List<OdbcRecord> list, final String fieldName) {

		Collections.sort(list, new Comparator<OdbcRecord>() {

			@Override
			public int compare(final OdbcRecord o1, final OdbcRecord o2) {
				return o1.getTimestamp(fieldName).compareTo(o2.getTimestamp(fieldName));
			}
		});
	}

	public static void sortListByYear(final List<OdbcRecord> list, final String fieldName) {

		Collections.sort(list, new Comparator<OdbcRecord>() {

			@Override
			public int compare(final OdbcRecord o1, final OdbcRecord o2) {
				return o1.getBigDecimal(fieldName).compareTo(o2.getBigDecimal(fieldName));
			}
		});
	}

	public static String getTypeOfBusiness(final OdbcRecord aRecord, final AbstractLookupTable lookupTableName) {

		if ("PROPTTY".equals(lookupTableName.get(aRecord.getField("SCREEN_TYPE").toString() + aRecord.getField("TREATY_TYPEX1").toString()))) {
			return "PROPTTY";
		} else if ("NONPROPTTY".equals(lookupTableName.get(aRecord.getField("SCREEN_TYPE").toString() + aRecord.getField("TREATY_TYPEX1").toString()))) {
			return "NONPROPTTY";
		} else {
			return null;
		}
	}

	public static boolean isEqual(final BigDecimal b1, final Integer b2) {
		return b1.compareTo(new BigDecimal(b2)) == 0;
	}

	public static String convertDateToString(final Timestamp timestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(timestamp);
		String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
		String date = String.valueOf(cal.get(Calendar.DATE));
		if (month.length() != 2) {
			month = "0" + month;
		}
		if (date.length() != 2) {
			date = "0" + date;
		}
		return String.valueOf(cal.get(Calendar.YEAR)) + month + date;
	}

}
