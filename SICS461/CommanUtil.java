package com.csc.sics.mtk.etl.util;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import com.csc.sics.mtk.etl.io.OdbcRecord;

public class CommanUtil {

	public static String getString(final OdbcRecord odbcRecord, final String fieldName, final String defaultValue) {
		return isNullOrBlank(odbcRecord, fieldName) ? defaultValue.trim() : odbcRecord.getString(fieldName).trim();
	}

	public static String getString(final OdbcRecord odbcRecord, final String fieldName) {
		return isNullOrBlank(odbcRecord, fieldName) ? "" : odbcRecord.getString(fieldName).trim();
	}

	public static boolean isNullOrBlank(final OdbcRecord odbcRecord, final String fieldName) {
		return odbcRecord.getField(fieldName) == null || odbcRecord.getField(fieldName).equals("");
	}

	public static boolean isNull(final OdbcRecord odbcRecord, final String fieldName) {
		return odbcRecord.getField(fieldName) == null;
	}

	public Calendar add1900ToYearOfDate(final Timestamp cvfDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(cvfDate.getTime());
		cal.add(Calendar.YEAR, 1900);
		return cal;
	}

	public Date getIpEndDate(final OdbcRecord odbcRecord, final OdbcRecord nextRecord) {
		Calendar calendar = Calendar.getInstance();
		if (nextRecord != null) {
			calendar.setTime(this.add1900ToYearOfDate(nextRecord.getTimestamp("CVF_DATE")).getTime());
			calendar.add(Calendar.DATE, -1);

		} else {
			calendar.setTime(this.add1900ToYearOfDate(odbcRecord.getTimestamp("CVF_DATE")).getTime());
			calendar.add(Calendar.YEAR, 1);
			calendar.add(Calendar.DATE, -1);
		}
		return ;
	}

}
